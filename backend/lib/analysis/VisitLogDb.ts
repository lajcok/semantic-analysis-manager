import {IVisitLog, VisitLog} from 'semantic-analysis/lib/tools/batch/VisitLog'
import * as schema from '../models/schema'

/**
 * Visit log persisted in database
 */
export class VisitLogDb implements IVisitLog {

    /* Properties */

    private readonly logBase = new VisitLog

    /* Initialization */

    readonly initiate = (): Promise<void> =>
        schema.Result.findAll({attributes: ['url']})
            .then((urls: {url: string}[]) =>
                urls.forEach(({url}) => this.logBase.add(url))
            )

    /* Visit Log */

    readonly add = (url: string) => this.logBase.add(url)

    readonly has = (url: string) => this.logBase.has(url)

    readonly siteCount = (url: string) => this.logBase.siteCount(url)

}
