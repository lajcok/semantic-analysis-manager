import {sequelize, Item, Log, Meta, Property, Result, Site} from './schema'
import {Transaction} from 'sequelize'
import * as fs from 'fs'
import {
    IExportable, ILogSimple, ITaskArchived, ITaskBase,
    IResultSimple,
    IItemSimple,
    IPropertySimple,
    ValueSimple
} from 'semantic-analysis'
import {createURL} from 'semantic-analysis/lib/util/url'

/*** Result Persistent storage in databse ***/

/* Helper functions */
const itemProperties = (item: IItemSimple): ReadonlyArray<{ name: string, property: IPropertySimple }> =>
    Object.keys(item.properties)
        .map(name =>
            (item.properties[name] as ReadonlyArray<IPropertySimple>)
                .map(property => ({name, property}))
        )
        .flat()

const flattenItems = (items: ReadonlyArray<IItemSimple>): ReadonlyArray<IItemSimple> =>
    items.length > 0 ? items.concat(flattenItems(
        items.map(item =>
            itemProperties(item)
                .map(({property}) => property.value as ValueSimple)
                .filter(value => typeof value === 'object') as ReadonlyArray<IItemSimple>
        ).flat()
    )) : []

/**
 * Store result with its items, properties and logs in database
 * @param result Result to store
 * @param additional Additional info like export paths and timestamps
 */
export const storeResult = (result: IResultSimple, additional?: Partial<ITaskArchived>) =>
    // transaction initiation and piping through inserting records
    sequelize.transaction((transaction: Transaction) =>
        Site.findOrCreate({
            where: {host: createURL(result.document.url ?? '')?.host}
        })
            .then(([siteEntry, ..._]: any[]) =>
                Result.create({
                    url: result.document.url,
                    ...(additional?.exportInfo ?? additional?.exportInfo ?? {}),
                    done: additional?.done,
                    siteId: siteEntry?.id,
                }, {transaction})
                    .then((resultEntry: any) => ({
                        entries: {siteEntry, resultEntry},
                        result,
                        flatItems: flattenItems(result.items),
                    }))
            )
            .then((attrs: any) =>
                Item.bulkCreate(
                    attrs.flatItems.map((item: IItemSimple) => ({
                        types: item.type,
                        itemUri: item.id,
                        resultId: attrs.entries.resultEntry.id,
                    })), {transaction}
                )
                    .then((itemEntries: any[]) => ({
                        ...attrs,
                        entries: {...attrs.entries, itemEntries},
                    }))
            )
            .then((attrs: any) =>
                Property.bulkCreate(
                    attrs.flatItems.map((item: IItemSimple, idx: number) =>
                        itemProperties(item)
                            .map(({name, property}) =>
                                ({name, value: property.value as ValueSimple})
                            )
                            .map(({name, value}) => ({
                                name,
                                itemId: attrs.entries.itemEntries[idx].id,
                                value_item: (
                                    typeof value === 'object'
                                        ? attrs.entries.itemEntries[attrs.flatItems.indexOf(value)]?.id
                                        : undefined
                                ),
                                value_string: typeof value !== 'object' ? value : undefined,
                            }))
                    ).flat(), {transaction}
                )
                    .then((propEntries: any[]) => ({
                            ...attrs,
                            entries: {
                                ...attrs.entries,
                                propEntries,
                            },
                        })
                    )
            )
            .then((attrs: any) => {
                const metaInput: any = {
                    result: [{
                        resultId: attrs.entries.resultEntry.id,
                    }],
                    items: attrs.flatItems.map((item: IItemSimple, idx: number) => ({
                        type: item.meta.type,
                        itemId: attrs.entries.itemEntries[idx].id,
                    })),
                    props: attrs.flatItems
                        .map((item: IItemSimple) =>
                            itemProperties(item).map(({property}) => property)
                        )
                        .flat()
                        .map((property: IPropertySimple, propIdx: number) => ({
                            type: property.meta.type,
                            propertyId: attrs.entries.propEntries[propIdx].id,
                        })),
                }
                return Meta.bulkCreate(
                    Object.keys(metaInput).map(k => metaInput[k]).flat(),
                    {transaction},
                )
                    .then((metaEntries: any[]) => ({
                        ...attrs,
                        entries: {
                            ...attrs.entries,
                            metaResult: metaEntries[0],
                            metaItems: metaEntries.slice(1, 1 + metaInput.items.length),
                            metaProps: metaEntries.slice(1 + metaInput.items.length),
                        },
                    }))
            })
            .then((attrs: any) =>
                Log.bulkCreate([
                    ...attrs.result.logs.map((log: ILogSimple) => ({
                        ...log,
                        metaId: attrs.entries.metaResult.id,
                        resultId: attrs.entries.resultEntry.id,
                    })),
                    ...attrs.flatItems.map((item: IItemSimple, idx: number) =>
                        item.meta.logs.map(log => ({
                            ...log,
                            metaId: attrs.entries.metaItems[idx].id,
                            resultId: attrs.entries.resultEntry.id,
                        }))
                    ).flat(),
                    ...attrs.flatItems.map((item: IItemSimple) =>
                        itemProperties(item).map(({property}, idx) =>
                            property.meta.logs.map(log => ({
                                ...log,
                                metaId: attrs.entries.metaProps[idx].id,
                                resultId: attrs.entries.resultEntry.id,
                            }))
                        ).flat()
                    ).flat()
                ], {transaction})
                    .then((logEntries: any) => ({
                        ...attrs,
                        entries: {
                            ...attrs.entries,
                            logEntries,
                        }
                    }))
            )
    )

/* Result querying */

export interface IListOptionsPartial {
    id?: number
    paginate: Partial<IPaginate>
    siteId?: number
}

export interface IListOptions extends IListOptionsPartial {
    paginate: IPaginate
}

export interface IPaginate {
    page: number
    perPage: number
}

export type ITaskId = Pick<ITaskBase, 'url' | 'done' | 'exportInfo'> & {
    id: number
    results?: IResultSimple
    stats: {
        itemsTotal: number
        correctness: number
    }
}

export interface ITaskListing {
    total: number
    options: IListOptions
    tasks: readonly ITaskId[]
}

/**
 * SQL Query constructor
 * @param id Results ID
 * @param paginate Pagination info
 * @param siteId Site filter
 * @returns string The query
 */
export const resultsQuery = ({id, paginate, siteId}: Partial<IListOptions>) => `
        WITH resultErrors AS (
            SELECT m."resultId"
            FROM meta m
                     INNER JOIN logs l
                                ON l."metaId" = m.id
            WHERE m."resultId" IS NOT NULL
              AND l.sort IN ('Error', 'Warning')
            GROUP BY m."resultId"
        ),
             itemErrors AS (
                 SELECT i."resultId", COUNT(DISTINCT i.id) AS errors
                 FROM items i
                          INNER JOIN meta m
                                     ON i.id = m."itemId"
                          INNER JOIN logs l
                                     ON m.id = l."metaId"
                 WHERE l.sort IN ('Error', 'Warning')
                 GROUP BY i."resultId"
             ),
             itemsTotal AS (
                 SELECT "resultId", COUNT(id) AS total
                 FROM items
                 GROUP BY "resultId"
             ),
             propErrors AS (
                 SELECT i."resultId", COUNT(DISTINCT p.id) AS errors
                 FROM items i
                          INNER JOIN properties p
                                     ON i.id = p."itemId"
                          INNER JOIN meta m
                                     ON p.id = m."propertyId"
                          INNER JOIN logs l
                                     ON m.id = l."metaId"
                 WHERE l.sort IN ('Error', 'Warning')
                 GROUP BY i."resultId"
             ),
             propsTotal AS (
                 SELECT i."resultId", COUNT(DISTINCT p.id) AS total
                 FROM properties p
                          INNER JOIN items i
                                     ON p."itemId" = i.id
                 GROUP BY i."resultId"
             )
        SELECT r.id,
               r.url,
               r.done,
               r."siteId",
               COALESCE(it.total, 0)::INT AS "itemsTotal",
               1 - ((re."resultId" IS NOT NULL)::INT + COALESCE(ie.errors, 0) + COALESCE(pe.errors, 0))::FLOAT
                   / (1 + it.total + COALESCE(pt.total, 0))::FLOAT AS "correctness"
        FROM results r
                 LEFT JOIN resultErrors re
                           ON r.id = re."resultId"
                 LEFT JOIN itemErrors ie
                           ON r.id = ie."resultId"
                 LEFT JOIN itemsTotal it
                           ON r.id = it."resultId"
                 LEFT JOIN propErrors pe
                           ON r.id = pe."resultId"
                 LEFT JOIN propsTotal pt
                           ON r.id = pt."resultId"
        WHERE TRUE
        ${id ? 'AND r.id = :id' : ''}
        ${siteId ? 'AND r."siteId" = :siteId' : ''}
        ORDER BY r.done DESC
        ${paginate ? 'LIMIT :limit OFFSET :offset' : ''}
`

/**
 * Queries results
 * @param options General options (ID, pagination, site filter)
 */
export const queryResults = (options?: Partial<IListOptions>) =>
    sequelize.query(resultsQuery(options ?? {}) + ';', {
        model: Result,
        mapToModel: true,
        replacements: {
            id: options?.id,
            limit: options?.paginate?.perPage,
            offset: options && options.paginate && options.paginate.page && options.paginate.perPage
                ? (options.paginate.page - 1) * options.paginate.perPage : undefined,
            siteId: options?.siteId,
        },
    })

/**
 * Results listing
 * @param _options Pagination, site filter
 */
export const listResults = (_options?: Partial<IListOptionsPartial>): Promise<ITaskListing> => {
    const options: IListOptions = {
        paginate: {
            page: _options?.paginate?.page ?? 1,
            perPage: _options?.paginate?.perPage ?? 25,
        },
        siteId: _options?.siteId,
    }
    return Promise.all([
        Result.count({
            where: options.siteId ? {siteId: options.siteId} : undefined,
        }),
        queryResults(options),
    ])
        .then(([total, resultEntries]: [number, any[]]): ITaskListing => ({
            total, options,
            tasks: resultEntries.map(entry => formatResult({
                ...entry.dataValues,
                stats: entry.dataValues,
            }, true)),
        }))
}

/**
 * Result detail with items and properties
 * @param id ID
 */
export const getResult = (id: number): Promise<ITaskId | undefined> =>
    Promise.all([
        Result.findByPk(id, {
            include: [
                {model: Meta, include: Log},
                {
                    model: Item, include: [
                        {model: Property, include: {model: Meta, include: Log}},
                        {model: Meta, include: Log},
                    ]
                },
            ],
        }),
        queryResults({id}),
    ])
        .then(([resultTree, [resultStats]]) => (
            {...resultTree.dataValues, stats: resultStats.dataValues}
        ))
        .then((resultEntry: any): ITaskId | undefined =>
            resultEntry ? formatResult(resultEntry, false) : undefined
        )

/**
 * Helper for formatting result into a desired interface
 * @param resultEntry
 * @param onlyTask Whether to construct only task or the whole item tree
 */
const formatResult = (resultEntry: any, onlyTask: boolean): ITaskId => ({
    id: resultEntry.id,
    url: resultEntry.url,
    done: resultEntry.done,
    exportInfo: resultEntry.json && resultEntry.html
        ? {json: resultEntry.json, html: resultEntry.html}
        : undefined,
    results: onlyTask ? undefined : {
        document: {url: resultEntry.url},
        logs: resultEntry.meta?.logs ?? [],
        items: buildItemHierarchy(resultEntry.items ?? []),
    },
    stats: {
        itemsTotal: resultEntry.stats.itemsTotal,
        correctness: resultEntry.stats.correctness,
    },
})

function buildItemHierarchy(itemEntries: ReadonlyArray<any>): readonly IItemSimple[] {
    const map = new Map<number, IItemSimple & { topLevel: boolean }>(
        itemEntries.map(itemEntry => [itemEntry.id, {
            type: itemEntry.types,
            id: itemEntry.itemUri,
            meta: itemEntry.meta,
            properties: {},
            topLevel: true,
        }])
    )

    itemEntries.forEach(itemEntry => {
        const item = map.get(itemEntry.id)!
        itemEntry.properties.forEach((propEntry: any) => {
            const child = map.get(propEntry.value_item)
            item.properties[propEntry.name] = [
                {
                    id: propEntry.id,
                    meta: propEntry.meta,
                    value: child ?? propEntry.value_string,
                } as IPropertySimple,
                ...(item.properties[propEntry.name] ?? [])
            ]
            if (child) {
                child.topLevel = false
            }
        })
    })

    return Array.from(map.values()).filter(item => item.topLevel)
}


// TODO move to a separate test file
if (require.main === module) {
    const path = require('path')

    const file = '1590671367737_www_czc_cz__'
    const [html, json] = [file + '.html', file + '.json']
    const exported = JSON.parse(
        fs.readFileSync(path.join('./exports/', json), 'utf8')
    ) as IExportable

    storeResult(exported.results, {exportInfo: {html, json}})
        .then((res: any) => console.log('success\n', res))
        .catch((error: any) => console.error('fail\n', error))
}
