import * as React from 'react'

interface IProps {
    up?: boolean
    onToggle?: (up: boolean) => void
}

interface IState {
    hover: boolean
}

export class JobIndicator extends React.Component<IProps, IState> {

    /* Initialization */

    constructor(props: IProps) {
        super(props)
        this.state = {
            hover: false
        }
    }

    /* View */

    private readonly clickable = () =>
        !!this.props.onToggle && this.props.up !== undefined

    readonly render = () =>
        (([badge, text, hoverText]: [string, string, string]) =>
                <span title="Job status" aria-label="Job status"
                      className={[
                          'badge',
                          !this.state.hover ? `badge-${badge}` : 'badge-info',
                      ].join(' ')}
                      onMouseEnter={() => this.setState({hover: this.clickable() && true})}
                      onMouseLeave={() => this.setState({hover: false})}
                      style={{
                          cursor: this.clickable() ? 'pointer' : 'default',
                          width: '3.5em',
                      }}
                      onClick={() => this.clickable() && this.props.onToggle?.(!this.props.up)}
                >{!this.state.hover ? text : hoverText}</span>
        )(settings(this.props.up))

}

const settings = (up?: boolean): [string, string, string] =>
    up !== undefined
        ? (
            up
                ? ['success', 'up', 'stop']
                : ['danger', 'down', 'run']
        )
        : ['light', 'N/A', 'N/A']
