/**
 * Migration script to transfer FS exports to database
 */

import * as fs from 'fs'
import * as path from 'path'
import * as db from './models/db'
import {IResultSimple} from 'semantic-analysis'

const exportsPath = process.argv[2]
const dirEnt = fs.opendirSync(exportsPath)

const readEntry = (): any =>
    dirEnt.read()
        .then(entry => {
            if (!entry) {
                throw 'entry null'
            }
            if (path.extname(entry.name) !== '.json') {
                // skip
                return readEntry()
            }

            const fileName = entry.name
            const filePath = path.join(exportsPath, fileName)
            const contents = fs.readFileSync(filePath, 'utf8')
            const {results}: { results: IResultSimple } = JSON.parse(contents)

            console.debug('parsed', fileName)
            const baseName = path.basename(fileName)
            const [html, json] = [baseName + '.json', baseName + '.html']
            return db.storeResult(results, {exportInfo: {html, json}})
        })
        .then((result: any) => {
            console.debug('done', result?.id)
            return readEntry()
        })
        .catch(reason => console.debug('the end?', reason))

if (module === require.main) {
    readEntry().then(() => console.debug('RLY THE END'))
}
