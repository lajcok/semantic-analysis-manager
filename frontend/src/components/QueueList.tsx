import * as React from 'react'
import {IBatchStateSimple} from 'semantic-analysis-manager/lib/analysis/analysis'

interface IProps {
    tasks?: IBatchStateSimple
}

export const QueueList: React.FunctionComponent<IProps> = props =>
    <section className="card">
        <h5 className="card-header">Queue</h5>
        {props.tasks
            ? <div>
                <h6 className="card-body card-subtitle text-muted small pb-1">
                    Running <span className="badge badge-info">{props.tasks.runningTotal}</span>
                </h6>
                <ul className="list-group list-group-flush">
                    {props.tasks.running.map((task, idx) =>
                        <li key={`running-${idx}`} className="list-group-item p-2 list-group-item-info">
                            {task.url}
                        </li>
                    )}
                </ul>
                <h6 className="card-body card-subtitle text-muted small pb-1">
                    Queued <span className="badge badge-light">{props.tasks.queuedTotal}</span>
                </h6>
                <ul className="list-group list-group-flush">
                    {props.tasks.queued.map((task, idx) =>
                        <li key={`queued-${idx}`} className="list-group-item p-2 list-group-item-light">
                            {task.url}
                        </li>
                    )}
                </ul>
                <h6 className="card-body card-subtitle text-muted small pb-1">
                    Failed <span className="badge badge-danger">{props.tasks.failedTotal}</span>
                </h6>
                <ul className="list-group list-group-flush">
                    {props.tasks.failed.map((task, idx) =>
                        <li key={`failed-${idx}`} className="list-group-item p-2 list-group-item-danger">
                            {task.url}
                        </li>
                    )}
                </ul>
                <h6 className="card-body card-subtitle text-muted small pb-1">
                    Done <span className="badge badge-success">{props.tasks.doneTotal}</span>
                </h6>
                <ul className="list-group list-group-flush">
                    {props.tasks.done.map((task, idx) =>
                        <li key={`done-${idx}`} className="list-group-item p-2 list-group-item-success">
                            {task.url}
                        </li>
                    )}
                </ul>
            </div>
            : <p className="card-body">No entries</p>
        }
    </section>
