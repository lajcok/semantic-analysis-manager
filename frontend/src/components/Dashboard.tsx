import * as React from 'react'
import {JobControl} from './JobControl'
import {QueueList} from './QueueList'
import {Api} from '../api'
import {IJobReport} from 'semantic-analysis-manager/lib'
import {ResultsList} from './ResultsList'
import {ITaskListing} from 'semantic-analysis-manager/lib/models/db'
import {IBatchStateSimple, IJobOptions} from 'semantic-analysis-manager/lib/analysis/analysis'

interface IProps {
    readonly api: Api
    up?: boolean
    tasks?: IBatchStateSimple
    updateJobState?: (report: IJobReport) => void
}

interface IState {
    listing?: ITaskListing
}

export class Dashboard extends React.Component<IProps, IState> {

    /* Initialization */

    constructor(props: IProps) {
        super(props)
        this.state = {}
    }

    /* Lifecycle */

    componentDidMount() {
        this.props.api.results()
            .then(listing => this.setState({listing}))
    }

    /* Events */

    readonly onSubmit = (options: IJobOptions) => {
        console.debug('options submit', options)
        return this.props.api.up(options)
            .then(this.props.updateJobState)
            .then(() => true)
            .catch(() => false)
    }

    /* View */

    readonly render = () =>
        <section>
            <h2>Dashboard</h2>
            <QueueList tasks={this.props.tasks}/>
            <JobControl up={this.props.up} onSubmit={this.onSubmit}/>
            <ResultsList listing={this.state.listing} browseAll={true}/>
        </section>

}
