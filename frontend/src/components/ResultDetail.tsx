import * as React from 'react'
import {Api} from '../api'
import {Spinner} from './Spinner'
import {ITaskId} from 'semantic-analysis-manager/lib'
import {ResultPreview} from './ResultPreview'

interface IProps {
    readonly api: Api
    readonly id: number
}

interface IState {
    task?: ITaskId
    loading: boolean
}

export class ResultDetail extends React.Component<IProps, IState> {

    /* Initialization */

    constructor(props: IProps) {
        super(props)
        this.state = {
            loading: true,
        }
    }

    /* Lifecycle */

    componentDidMount() {
        this.props.api.result(this.props.id)
            .then(task => {
                console.debug('result of a task', task)
                this.setState({
                    task,
                    loading: false,
                })
            })
    }

    /* View */

    readonly render = () =>
        <section>
            <h3>Result Detail</h3>
            {this.state.loading ? <Spinner/> : (
                this.state.task
                    ? <div>
                        <table className="table table-responsive table-sm table-borderless">
                            <tbody>
                            <tr>
                                <th scope="row">URL</th>
                                <td>
                                    <a href={this.state.task.url} target="_blank">{
                                        this.state.task.url
                                    }</a>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Items</th>
                                <td>{this.state.task.stats.itemsTotal}</td>
                            </tr>
                            <tr>
                                <th scope="row">Correctness</th>
                                <td className={
                                    this.state.task.stats.correctness ?? undefined
                                        ? (this.state.task.stats.correctness >= 1 ? 'text-success'
                                            : (this.state.task.stats.correctness >= .8 ? 'text-warning' : 'text-danger')
                                        ) : undefined
                                }
                                >{this.state.task.stats.correctness
                                    ? `${Math.round(this.state.task.stats.correctness * 100)} %`
                                    : <em className="text-muted">&mdash;</em>
                                }</td>
                            </tr>
                            <tr>
                                <th scope="row">Timestamp</th>
                                <td>{this.state.task.done.toLocaleString()}</td>
                            </tr>
                            </tbody>
                        </table>
                        <section>
                            <h4>Items</h4>
                            {this.state.task.results
                                ? <ResultPreview {...this.state.task.results}/>
                                : <p>No result found</p>
                            }
                        </section>
                    </div> : <p>Result could not be found</p>
            )}
        </section>

}
