import {
    BatchAnalyzer,
    CrawlAnalyzer,
    IBatchAnalyzer,
    IBatchState,
    ICrawlOptions,
    IOptions,
    ITaskDone
} from 'semantic-analysis'
import {IJobOptions} from './analysis'

export type ILazyOptions = IOptions & ICrawlOptions & Omit<IJobOptions, 'urls'>

/**
 * Lazy kind of analyzer, collects options and waits with starting unless it is necessary
 */
export class LazyAnalyzer implements IBatchAnalyzer {

    /* Properties */

    private options: ILazyOptions = {
        rateLimit: 500,
        maxRunning: 1,
    }

    private _analyzer?: IBatchAnalyzer
    private analyzer(init = false): IBatchAnalyzer | undefined {
        if (!this._analyzer && init) {
            console.debug('initiating ', this.options.crawl ? 'crawl' : 'batch')
            this._analyzer = new (this.options.crawl ? CrawlAnalyzer : BatchAnalyzer)(this.options)
        }
        return this._analyzer
    }

    readonly up = () => !!this._analyzer

    /* Initialization */

    constructor(options?: Partial<ILazyOptions>) {
        this.passOptions(options ?? {})
    }

    passOptions(options: Partial<ILazyOptions>) {
        console.debug('lazy got options', options)
        if (this.up()) {
            throw new Error('Analyzer has already been instantiated')
        }
        this.options = {
            ...this.options,
            ...options,
        }
    }

    /* Analyzer */

    get queued() {
        return this.analyzer()?.queued ?? []
    }

    get running() {
        return this.analyzer()?.running ?? []
    }

    get done() {
        return this.analyzer()?.done ?? []
    }

    get failed() {
        return this.analyzer()?.failed ?? []
    }

    readonly addTask = (url: string, source?: string): Promise<ITaskDone> =>
        this.analyzer(true)!.addTask(url, source)

    readonly state = () => this.analyzer()?.state() ?? {
        queued: this.queued,
        running: this.running,
        done: this.done,
        failed: this.failed,
    }
    readonly dispose = () => this.analyzer()?.dispose?.()

}