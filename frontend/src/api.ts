import {AxiosInstance} from 'axios'
import {IJobReport, IPaginate, ITaskId, ITaskListing} from 'semantic-analysis-manager/lib'
import {IJobOptions} from 'semantic-analysis-manager/lib/analysis/analysis'
import {IHistData, ISiteCorrectness, ISiteStat, ITypeStat} from 'semantic-analysis-manager/lib/models/statistics'

export class Api {

    /* Initialization */

    constructor(private readonly axios: AxiosInstance) {}

    /* Interface */

    readonly report = (): Promise<IJobReport> =>
        this.axios.get<IJobReport>('job')
            .then(({data}) => data)

    readonly up = (options?: IJobOptions): Promise<IJobReport> =>
        this.axios.post<IJobReport>('job', options ?? {})
            .then(({data}) => data)

    readonly down = (): Promise<IJobReport> =>
        this.axios.delete<IJobReport>('job')
            .then(({data}) => data)

    readonly results = (paginate?: Partial<IPaginate>, siteId?: number): Promise<ITaskListing> =>
        this.axios.get<ITaskListing>('results', {
            params: {...paginate, siteId}
        })
            .then(({data}) => ({
                ...data,
                tasks: data.tasks.map(parseTask),
            }))

    readonly result = (id: number): Promise<ITaskId> =>
        this.axios.get<ITaskId>(`results/${id}`)
            .then(({data}) => parseTask(data))

    readonly siteStats = (): Promise<readonly ISiteStat[]> =>
        this.axios.get<readonly ISiteStat[]>('stats/site')
            .then(({data}) => data)

    readonly typeStats = (): Promise<readonly ITypeStat[]> =>
        this.axios.get<readonly ITypeStat[]>('stats/type')
            .then(({data}) => data)

    readonly correctnessStats = (): Promise<readonly ISiteCorrectness[]> =>
        this.axios.get<readonly ISiteCorrectness[]>('stats/correctness/site')
            .then(({data}) => data)

    readonly correctnessHist = (): Promise<readonly IHistData[]> =>
        this.axios.get<readonly IHistData[]>('stats/correctness/hist')
            .then(({data}) => data)

}

const parseTask = (task: ITaskId): ITaskId => ({
    ...task,
    done: new Date(task.done),

})
