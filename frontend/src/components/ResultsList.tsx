import * as React from 'react'
import {Link} from 'react-router-dom'
import {Spinner} from './Spinner'
import {ITaskListing} from 'semantic-analysis-manager/lib'
import {Paginate} from './Paginate'

interface IProps {
    listing?: ITaskListing
    // sites?: readonly {id: number, host: string}[]
    browseAll?: boolean
}

// TODO site selection

export const ResultsList: React.FunctionComponent<IProps> = ({listing, browseAll}) =>
    <section className="card">
        <h5 className="card-header">Results</h5>
        {listing ? (
            <div>
                <div className="card-body text-muted small">
                    Listing <strong>{(listing.options.paginate.page - 1) * listing.options.paginate.perPage + 1}</strong>-
                    <strong>{
                        Math.min(listing.options.paginate.page * listing.options.paginate.perPage, listing.total)
                    }</strong> of <strong>{listing.total}</strong> results
                    {browseAll ? (
                        <span>
                            {} &middot; <Link className="card-link" to="/results">Browse all</Link>
                        </span>
                    ) : undefined}
                </div>
                {listing.tasks.length
                    ? <table className="table table-hover table-sm">
                        <thead>
                        <tr>
                            <th scope="col">URL</th>
                            <th scope="col">Items</th>
                            <th scope="col">Correctness</th>
                            <th scope="col">Timestamp</th>
                        </tr>
                        </thead>
                        <tbody>{
                            listing.tasks.map(({id, url, done, stats}) =>
                                <tr key={id}
                                    style={{
                                        opacity: !stats.itemsTotal ? .3 : undefined,
                                    }}
                                >
                                    <td style={{wordBreak: 'break-all'}}>
                                        <Link to={`/results/${id}`}>{url}</Link>
                                    </td>
                                    <td className="text-center">{stats.itemsTotal}</td>
                                    <td className={[
                                        'text-center',
                                        stats.correctness != undefined
                                            ? (stats.correctness >= 1 ? 'text-success'
                                                : (stats.correctness >= .8 ? 'text-warning' : 'text-danger')
                                            ) : undefined,
                                    ].join(' ')}
                                    >{stats.correctness
                                        ? `${Math.round(stats.correctness * 100)} %`
                                        : <em className="text-muted">&mdash;</em>
                                    }</td>
                                    <td className="text-nowrap">{done.toLocaleString()}</td>
                                </tr>
                            )
                        }</tbody>
                    </table>
                    : <p className="card-body">No results found</p>
                }
                <div className="card-footer">
                    <Paginate {...listing.options.paginate} total={listing.total} linkTo="/results"/>
                </div>
            </div>
        ) : <Spinner/>}
    </section>
