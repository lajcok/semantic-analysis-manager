import * as React from 'react'

export const Spinner = ({small}: {small?: boolean}) =>
    <div className={['spinner-border', 'm-1', small ? 'spinner-border-sm' : ''].join(' ')}
         role="status">
        <span className="sr-only">Loading...</span>
    </div>