const path = require('path');

module.exports = {
    mode: 'development',
    devtool: 'source-map',
    entry: './src/',
    resolve: {
        extensions: ['.ts', '.tsx', '.js'],
    },
    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                exclude: /node_modules/,
                use: 'ts-loader',
            },
            {
                test: /\.htm(l?)$/,
                loader: 'file-loader?name=[name].[ext]',
            },
            {
                test: /\.css$/,
                loader: 'file-loader?name=[name].[ext]',
            },
        ]
    },
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'dist'),
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 3000,
        proxy: {
            '/api': {
                target: 'http://localhost:8080',
                pathRewrite: {'^/api': ''},
            },
        },
        historyApiFallback: {
            index: '/',
        },
    }
};
