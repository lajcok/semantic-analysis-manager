import * as React from 'react'
import {IPaginate, ITaskListing} from 'semantic-analysis-manager/lib'
import {Api} from '../api'
import {ResultsList} from './ResultsList'
import {Statistics} from './Statistics'

interface IProps {
    api: Api
    paginate?: Partial<IPaginate>
    siteId?: number
}

interface IState {
    paginate: IPaginate
    listing?: ITaskListing
}

const PAGINATE_DEFAULTS: IPaginate = {
    page: 1,
    perPage: 15,
}

export class ResultBrowser extends React.Component<IProps, IState> {

    /* Initialization */

    constructor(props: IProps) {
        super(props)
        this.state = {
            paginate: {
                page: this.props.paginate?.page ?? PAGINATE_DEFAULTS.page,
                perPage: this.props.paginate?.perPage ?? PAGINATE_DEFAULTS.perPage,
            }
        }
    }

    componentDidMount() {
        this.props.api.results(this.state.paginate, this.props.siteId)
            .then(listing => this.setState({listing}))
    }

    componentDidUpdate({paginate}: Readonly<IProps>, prevState: Readonly<IState>) {
        const currentPaginate = {
            page: this.props.paginate?.page ?? PAGINATE_DEFAULTS.page,
            perPage: this.props.paginate?.perPage ?? PAGINATE_DEFAULTS.perPage,
        }
        if (prevState.paginate.page === currentPaginate.page && prevState.paginate.perPage === currentPaginate.perPage) {
            return
        }

        this.setState({listing: undefined, paginate: currentPaginate}, () =>
            this.props.api.results(this.state.paginate, this.props.siteId)
                .then(listing => this.setState({listing}))
        )
    }

    /* View */

    readonly render = () =>
        <section>
            <h2>Result Browser</h2>
            <Statistics api={this.props.api}/>
            <ResultsList listing={this.state.listing}/>
        </section>
}