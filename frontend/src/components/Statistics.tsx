import * as React from 'react'
import {Api} from '../api'
import {IHistData, ISiteCorrectness, ISiteStat, ITypeStat} from 'semantic-analysis-manager/lib/models/statistics'
import {Spinner} from './Spinner'
import Chart from 'react-apexcharts'

interface IProps {
    api: Api
}

interface IState {
    siteStats?: readonly ISiteStat[]
    typeStats?: readonly ITypeStat[]
    correctnessSite?: readonly ISiteCorrectness[]
    correctnessHist?: readonly IHistData[]
    expanded: {
        [key: string]: boolean
    }
}

export class Statistics extends React.Component<IProps, IState> {

    /* Properties */

    private _heatmap?: ReturnType<typeof prepareHeatmapData>
    private readonly heatmapData = (stats: readonly ITypeStat[]) =>
        this._heatmap ?? (this._heatmap = prepareHeatmapData(stats))

    /* Initialization */

    constructor(props: IProps) {
        super(props)
        this.state = {
            expanded: {},
        }
    }

    componentDidMount() {
        this.props.api.siteStats()
            .then(siteStats => this.setState({siteStats}))
        this.props.api.typeStats()
            .then(typeStats => this.setState({typeStats}))
        this.props.api.correctnessStats()
            .then(correctnessSite => this.setState({correctnessSite}))
        this.props.api.correctnessHist()
            .then(correctnessHist => this.setState({correctnessHist}))
    }

    /* View */

    readonly toggle = (section: string) =>
        this.setState(({expanded}) => ({
            expanded: {
                ...expanded,
                [section]: !expanded[section],
            }
        }))


    readonly render = () =>
        <section className="card">
            <h5 className="card-header">Statistics</h5>
            <StatSection
                expanded={this.state.expanded.sites}
                toggle={() => this.toggle('sites')}
                title="Site Samples"
                subtitle={
                    this.state.siteStats
                        ? ((([total, semantic, errorResults]) =>
                                <p className="card-subtitle small text-muted mb-3">
                                    <strong>{semantic}</strong> semantic samples
                                    {} out of <strong>{total}</strong> total
                                    {} ({Math.round(semantic / total * 100)}% coverage),
                                    {} <strong>{errorResults}</strong> pages contain errors
                                    {} ({Math.round(errorResults / semantic * 100)}% of semantic documents)
                                </p>
                        )(
                        this.state.siteStats
                            .map(({total, semantic, errorResults}) => [
                                total, semantic, errorResults
                            ])
                            .reduce((previousValues, currentValues) =>
                                previousValues.map((value, index) =>
                                    value + currentValues[index]
                                )
                            )
                        ))
                        : <Spinner small/>
                }
            >{
                this.state.siteStats
                    ? <Chart type="bar"
                             options={{
                                 chart: {
                                     type: 'bar',
                                     stacked: true,
                                 },
                                 plotOptions: {
                                     bar: {horizontal: true},
                                 },
                                 title: {text: 'Sample count per sites'},
                                 xaxis: {
                                     type: 'category',
                                     categories: this.state.siteStats.map(stat => stat.host),
                                 },
                                 colors: ['#dc3545', '#28a745', '#ddd'],
                             }}
                             series={
                                 (stats =>
                                         (['error', 'semantic', 'empty'] as ['error', 'semantic', 'empty'])
                                             .map(name => ({
                                                 name, data: stats.map(s => s[name])
                                             }))
                                 )(this.state.siteStats.map(stat => ({
                                     empty: stat.total - stat.semantic,
                                     semantic: stat.semantic - stat.errorResults,
                                     error: stat.errorResults,
                                 })))
                             }
                    />
                    : <Spinner/>
            }</StatSection>
            <StatSection
                expanded={this.state.expanded.types}
                toggle={() => this.toggle('types')}
                title="Type Instances"
                subtitle={
                    this.state.typeStats
                        ? <p className="card-subtitle small text-muted mb-3">
                            <strong>{
                                this.state.typeStats
                                    .map(stat => stat.total)
                                    .reduce((sum, count) => sum + count)
                            }</strong> items discovered
                            {} of <strong>{
                            this.state.typeStats
                                .map(stat => stat.typeHost)
                                .filter((value, index, array) => array.indexOf(value) === index)
                                .length
                        }</strong> distinct types
                        </p>
                        : <Spinner small/>
                }
            >{
                this.state.typeStats
                    ? (({types, series}) =>
                            <Chart type="heatmap"
                                   options={{
                                       chart: {type: 'heatmap'},
                                       dataLabels: {enabled: false},
                                       title: {text: 'Frequency of item type on sites'},
                                       xaxis: {
                                           type: 'category',
                                           categories: types,
                                           labels: {
                                               rotate: -70,
                                           },
                                       },
                                       grid: {
                                           xaxis: {
                                               lines: {
                                                   show: true
                                               }
                                           }
                                       },
                                       stroke: {
                                           show: true,
                                           colors: ['#ddd'],
                                           width: .3,
                                       },
                                       plotOptions: {
                                           heatmap: {
                                               distributed: true,
                                               radius: 0,
                                           },
                                       },
                                   }}
                                   series={series}
                            />
                    )(this.heatmapData(this.state.typeStats))
                    : <Spinner/>
            }</StatSection>
            <StatSection
                expanded={this.state.expanded.correctnessHist}
                toggle={() => this.toggle('correctnessHist')}
                title="Correctness Distribution"
                subtitle={
                    this.state.correctnessHist
                        ? <p className="card-subtitle small text-muted mb-3">
                            Average correctness of
                            {} <strong>{
                            Math.floor((
                                this.state.correctnessHist
                                    .filter(stat => stat.bucket != undefined)
                                    .map(stat => ([stat.mean!, stat.frequency]))
                                    .reduce(([avg, sum], [mean, freq]) =>
                                        [(avg * sum + mean * freq) / (sum + freq), (sum + freq)]
                                    )[0]
                            ) * 100)
                        } %</strong>
                            {} with a minimum at <strong>{
                            Math.floor(this.state.correctnessHist
                                .map(stat => stat.interval[0] ?? Infinity)
                                .reduce((min, current) =>
                                    current < min ? current : min
                                ) * 100
                            )
                        } %</strong>
                            {} and a maximum at <strong>{
                            Math.floor(this.state.correctnessHist
                                .map(stat => stat.interval[0] ?? -Infinity)
                                .reduce((max, current) =>
                                    current > max ? current : max
                                ) * 100
                            )
                        } %</strong>
                        </p>
                        : <Spinner small/>
                }
            >{
                this.state.correctnessHist
                    ? <Chart type="histogram"
                             options={{
                                 chart: {type: 'histogram'},
                                 title: {text: 'Frequency of correctness values'},
                                 labels: this.state.correctnessHist.map(stat =>
                                     stat.mean != undefined
                                         ? `${Math.round(stat.mean * 100)} %`
                                         : 'none'
                                 ),
                             }}
                             series={[{
                                 name: 'Correctness',
                                 data: this.state.correctnessHist.map(stat => stat.frequency),
                             }]}
                    />
                    : <Spinner/>
            }</StatSection>
            <StatSection
                expanded={this.state.expanded.correctnessSite}
                toggle={() => this.toggle('correctnessSite')}
                title="Site Correctness"
                subtitle={
                    this.state.correctnessSite
                        ? <p className="card-subtitle small text-muted mb-3">
                            Comparison of average correctness
                            {} of <strong>{this.state.correctnessSite.length}</strong> sites
                        </p>
                        : <Spinner small/>
                }
            >{
                this.state.correctnessSite
                    ? <Chart type="bar"
                             options={{
                                 chart: {
                                     type: 'bar',
                                 },
                                 plotOptions: {
                                     bar: {
                                         horizontal: true,
                                         dataLabels: {
                                             position: 'top',
                                         },
                                     },
                                 },
                                 title: {text: 'Correctness average site comparison'},
                                 xaxis: {
                                     type: 'category',
                                     categories: this.state.correctnessSite.map(stat => stat.host),
                                     labels: {
                                         formatter: (v: number) =>
                                             `${Math.round(v * 100)} %`,
                                     },
                                     min: this.state.correctnessSite
                                         .map(stat => stat.min)
                                         .reduce((min, cur) => cur < min ? cur : min),
                                     max: this.state.correctnessSite
                                         .map(stat => stat.max)
                                         .reduce((max, cur) => cur > max ? cur : max),
                                 },
                                 dataLabels: {
                                     formatter: (v: number) =>
                                         Math.round(v * 100),
                                 },
                                 tooltip: {
                                     y: {
                                         formatter: (v: number) =>
                                             `${Math.round(v * 100)} %`,
                                     }
                                 },
                             }}
                             series={[{
                                 name: 'mean correctness',
                                 data: this.state.correctnessSite.map(stat => stat.mean),
                             }]}
                    />
                    : <Spinner/>
            }</StatSection>
        </section>

}

interface ISectionProps {
    expanded: boolean
    title: React.ReactNode
    subtitle: React.ReactNode
    children: React.ReactNode
    toggle: () => void
}

const StatSection: React.FunctionComponent<ISectionProps> = props =>
    <section>
        <header className="card-body">
            <h6 className="card-title" style={{cursor: 'pointer'}}
                onClick={props.toggle}
                title={props.expanded ? 'Collapse' : 'Expand'}
                aria-label={props.expanded ? 'Collapse' : 'Expand'}
            >
                {props.expanded ? '\u25B4' : '\u25BE'} {props.title}
            </h6>
            {props.subtitle}
        </header>
        {props.expanded ? props.children : undefined}
    </section>


const prepareHeatmapData = (typeStats: readonly ITypeStat[]) => {
    console.groupCollapsed('prepareHeatmapData')
    console.debug({typeStats})

    const [siteMap, typeMap] = (['siteHost', 'typeHost'] as ['siteHost', 'typeHost'])
        .map(key => new Map(typeStats
                .map(stat => stat[key])
                .sort()
                .filter((value, index, array) => array.indexOf(value) === index)
                .map((value, index) => [value, index])
            )
        )

    const [sites, types] = [
        Array.from(siteMap.keys()).map(s => s ?? 'unassigned'),
        Array.from(typeMap.keys())
            .map(t => t && t.startsWith('schema.org/') ? t.substr('schema.org/'.length) : t)
            .map(t => t ?? 'no type'),
    ]

    const series = sites.map(name => ({
        name, data: new Array<number>(typeMap.size).fill(0)
    }))

    console.debug({siteMap, typeMap, series})

    typeStats.forEach(stat => {
        const x = typeMap.get(stat.typeHost)!
        const y = siteMap.get(stat.siteHost)!
        console.debug({stat, x, y, series})
        series[y].data[x] += stat.total
    })

    console.groupEnd()
    return {sites, types, series}
}
