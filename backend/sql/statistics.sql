-- Site stats summary
EXPLAIN ANALYZE
WITH resultsTotal AS (
    SELECT "siteId", COUNT(*) AS "resultCount"
    FROM results
    GROUP BY "siteId"
),
     resultsUnempty AS (
         SELECT "siteId", COUNT(*) AS "resultCount"
         FROM results
         WHERE id IN (SELECT "resultId" FROM items)
         GROUP BY "siteId"
     ),
--      resultsWithErrors AS (
--          SELECT r."siteId", COUNT(*) AS "resultCount"
--          FROM results r
--          WHERE r.id IN (
--              SELECT "resultId"
--              FROM logs
--              WHERE sort IN ('Error', 'Warning')
--          )
--          GROUP BY "siteId"
--      ),
     errorLogs AS (
         SELECT r."siteId",
                COUNT(DISTINCT r.id) AS resultCount,
                COUNT(l.id)          AS logCount
         FROM results r
                  INNER JOIN logs l
                             ON r.id = l."resultId"
                                 AND l.sort IN ('Error', 'Warning')
         GROUP BY r."siteId"
     )
SELECT s.id,
       s.host,
       coalesce(rt."resultCount", 0) AS "resultsTotal",
       coalesce(ru."resultCount", 0) AS "resultsUnempty",
       coalesce(re.resultCount, 0)   AS "resultsWithErrors",
       coalesce(re.logCount, 0)      AS "errorLogs"
FROM sites AS s
         FULL JOIN resultsTotal AS rt
                   ON s.id = rt."siteId"
         FULL JOIN resultsUnempty AS ru
                   ON s.id = ru."siteId"
         FULL JOIN errorLogs AS re
                   ON s.id = re."siteId"
ORDER BY s.host;

-- Item types stats
EXPLAIN
WITH itemTypes AS (
    SELECT *, split_part(unnest(types), '://', 2) AS "itemType"
    FROM items
    UNION
    SELECT *, NULL AS "itemType"
    FROM items
    WHERE types = '{}'
)
SELECT s.host, i."itemType", COUNT(*) AS "itemCount"
FROM itemTypes i
         INNER JOIN results r
                    ON i."resultId" = r.id
         INNER JOIN sites s
                    ON r."siteId" = s.id
GROUP BY s.host, i."itemType"
ORDER BY s.host, i."itemType";

-- Item types average
EXPLAIN
WITH itemTypes AS (
    SELECT *, split_part(unnest(types), '://', 2) AS "itemType"
    FROM items
    UNION
    SELECT *, NULL AS "itemType"
    FROM items
    WHERE types = '{}'
),
     resultCounts AS (
         SELECT r.*, i."itemType", count(*) AS "itemCount"
         FROM itemTypes i
                  INNER JOIN results r
                             ON i."resultId" = r.id
         GROUP BY r.id, i."itemType"
         ORDER BY r.id, i."itemType"
     )
SELECT s.host,
       r."itemType",
       SUM("itemCount")::INT   AS "total",
       MIN("itemCount")::INT   AS "min",
       MAX("itemCount")::INT   AS "max",
       AVG("itemCount")::FLOAT AS "avg"
FROM resultCounts r
         INNER JOIN sites s
                    ON r."siteId" = s.id
GROUP BY s.host, r."itemType"
ORDER BY r."itemType" NULLS FIRST, s.host NULLS FIRST;

-- Correctness
EXPLAIN
WITH resultErrors AS (
    SELECT m."resultId"
    FROM meta m
             INNER JOIN logs l
                        ON l."metaId" = m.id
    WHERE m."resultId" IS NOT NULL
      AND l.sort IN ('Error', 'Warning')
    GROUP BY m."resultId"
),
     itemErrors AS (
         SELECT i."resultId", COUNT(DISTINCT i.id) AS errors
         FROM items i
                  INNER JOIN meta m
                             ON i.id = m."itemId"
                  INNER JOIN logs l
                             ON m.id = l."metaId"
         WHERE l.sort IN ('Error', 'Warning')
         GROUP BY i."resultId"
     ),
     itemsTotal AS (
         SELECT "resultId", COUNT(id) AS total
         FROM items
         GROUP BY "resultId"
     ),
     propErrors AS (
         SELECT i."resultId", COUNT(DISTINCT p.id) AS errors
         FROM items i
                  INNER JOIN properties p
                             ON i.id = p."itemId"
                  INNER JOIN meta m
                             ON p.id = m."propertyId"
                  INNER JOIN logs l
                             ON m.id = l."metaId"
         WHERE l.sort IN ('Error', 'Warning')
         GROUP BY i."resultId"
     ),
     propsTotal AS (
         SELECT i."resultId", COUNT(DISTINCT p.id) AS total
         FROM properties p
                  INNER JOIN items i
                             ON p."itemId" = i.id
         GROUP BY i."resultId"
     )
SELECT r.*,
       (re."resultId" IS NOT NULL)::INT                    AS "resultErrors",
       ie.errors::INT                                      AS "itemErrors",
       it.total::INT                                       AS "itemsTotal",
       pe.errors::INT                                      AS "propErrors",
       pt.total::INT                                       AS "propsTotal",
       1 - ((re."resultId" IS NOT NULL)::INT + COALESCE(ie.errors, 0) + COALESCE(pe.errors, 0))::FLOAT
           / (1 + it.total + COALESCE(pt.total, 0))::FLOAT AS "correctness"
FROM results r
         LEFT JOIN resultErrors re
                   ON r.id = re."resultId"
         LEFT JOIN itemErrors ie
                   ON r.id = ie."resultId"
         LEFT JOIN itemsTotal it
                   ON r.id = it."resultId"
         LEFT JOIN propErrors pe
                   ON r.id = pe."resultId"
         LEFT JOIN propsTotal pt
                   ON r.id = pt."resultId"
-- WHERE r.id = :id
ORDER BY r.done DESC
-- LIMIT :perPage OFFSET (:page - 1) * :perPage
;

-- Correctness Site Stats
WITH correctness AS (
    WITH resultErrors AS (
        SELECT m."resultId"
        FROM meta m
                 INNER JOIN logs l
                            ON l."metaId" = m.id
        WHERE m."resultId" IS NOT NULL
          AND l.sort IN ('Error', 'Warning')
        GROUP BY m."resultId"
    ),
         itemErrors AS (
             SELECT i."resultId", COUNT(DISTINCT i.id) AS errors
             FROM items i
                      INNER JOIN meta m
                                 ON i.id = m."itemId"
                      INNER JOIN logs l
                                 ON m.id = l."metaId"
             WHERE l.sort IN ('Error', 'Warning')
             GROUP BY i."resultId"
         ),
         itemsTotal AS (
             SELECT "resultId", COUNT(id) AS total
             FROM items
             GROUP BY "resultId"
         ),
         propErrors AS (
             SELECT i."resultId", COUNT(DISTINCT p.id) AS errors
             FROM items i
                      INNER JOIN properties p
                                 ON i.id = p."itemId"
                      INNER JOIN meta m
                                 ON p.id = m."propertyId"
                      INNER JOIN logs l
                                 ON m.id = l."metaId"
             WHERE l.sort IN ('Error', 'Warning')
             GROUP BY i."resultId"
         ),
         propsTotal AS (
             SELECT i."resultId", COUNT(DISTINCT p.id) AS total
             FROM properties p
                      INNER JOIN items i
                                 ON p."itemId" = i.id
             GROUP BY i."resultId"
         )
    SELECT r.id,
           r."siteId",
           1 - ((re."resultId" IS NOT NULL)::INT + COALESCE(ie.errors, 0) + COALESCE(pe.errors, 0))::FLOAT
               / (1 + it.total + COALESCE(pt.total, 0))::FLOAT AS "correctness"
    FROM results r
             LEFT JOIN resultErrors re
                       ON r.id = re."resultId"
             LEFT JOIN itemErrors ie
                       ON r.id = ie."resultId"
             LEFT JOIN itemsTotal it
                       ON r.id = it."resultId"
             LEFT JOIN propErrors pe
                       ON r.id = pe."resultId"
             LEFT JOIN propsTotal pt
                       ON r.id = pt."resultId"
)
SELECT s.host,
       MIN("correctness") AS "min",
       MAX("correctness") AS "max",
       AVG("correctness") AS "mean"
FROM correctness c
    INNER JOIN sites s
        ON c."siteId" = s.id
GROUP BY s.host
ORDER BY s.host;

-- Correctness Histogram
WITH correctness AS (
    WITH resultErrors AS (
        SELECT m."resultId"
        FROM meta m
                 INNER JOIN logs l
                            ON l."metaId" = m.id
        WHERE m."resultId" IS NOT NULL
          AND l.sort IN ('Error', 'Warning')
        GROUP BY m."resultId"
    ),
         itemErrors AS (
             SELECT i."resultId", COUNT(DISTINCT i.id) AS errors
             FROM items i
                      INNER JOIN meta m
                                 ON i.id = m."itemId"
                      INNER JOIN logs l
                                 ON m.id = l."metaId"
             WHERE l.sort IN ('Error', 'Warning')
             GROUP BY i."resultId"
         ),
         itemsTotal AS (
             SELECT "resultId", COUNT(id) AS total
             FROM items
             GROUP BY "resultId"
         ),
         propErrors AS (
             SELECT i."resultId", COUNT(DISTINCT p.id) AS errors
             FROM items i
                      INNER JOIN properties p
                                 ON i.id = p."itemId"
                      INNER JOIN meta m
                                 ON p.id = m."propertyId"
                      INNER JOIN logs l
                                 ON m.id = l."metaId"
             WHERE l.sort IN ('Error', 'Warning')
             GROUP BY i."resultId"
         ),
         propsTotal AS (
             SELECT i."resultId", COUNT(DISTINCT p.id) AS total
             FROM properties p
                      INNER JOIN items i
                                 ON p."itemId" = i.id
             GROUP BY i."resultId"
         )
    SELECT 1 - ((re."resultId" IS NOT NULL)::INT + COALESCE(ie.errors, 0) + COALESCE(pe.errors, 0))::FLOAT
        / (1 + it.total + COALESCE(pt.total, 0))::FLOAT AS "correctness"
    FROM results r
             LEFT JOIN resultErrors re
                       ON r.id = re."resultId"
             LEFT JOIN itemErrors ie
                       ON r.id = ie."resultId"
             LEFT JOIN itemsTotal it
                       ON r.id = it."resultId"
             LEFT JOIN propErrors pe
                       ON r.id = pe."resultId"
             LEFT JOIN propsTotal pt
                       ON r.id = pt."resultId"
)
SELECT width_bucket("correctness", 0., 1., 9) AS "bucket",
       MIN("correctness")                     AS "intervalLow",
       MAX("correctness")                     AS "intervalHigh",
       AVG("correctness")                     AS "mean",
       COUNT(*)::INT                          AS "frequency"
FROM correctness
GROUP BY "bucket"
ORDER BY "bucket" NULLS FIRST;
