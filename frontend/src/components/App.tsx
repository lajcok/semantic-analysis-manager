import * as React from 'react'
import {
    BrowserRouter as Router,
    Switch, Route, Link
} from 'react-router-dom'
import {JobIndicator} from './JobIndicator'
import {IJobReport} from 'semantic-analysis-manager/lib'
import {Api} from '../api'
import {Dashboard} from './Dashboard'
import {IBatchStateSimple} from 'semantic-analysis-manager/lib/analysis/analysis'
import {ResultDetail} from './ResultDetail'
import Axios from 'axios'
import {ResultBrowser} from './ResultBrowser'

const POLL_RATE = 1000

interface IState {
    up?: boolean
    tasks?: IBatchStateSimple
}

export class App extends React.Component<{}, IState> {

    /* Properties */

    private readonly api: Api
    private poll: boolean = true

    /* Initialization */

    constructor(props: {}) {
        super(props)
        this.state = {}
        this.api = new Api(Axios.create({
            baseURL: '/api',
            // timeout: 500,
        }))
    }

    /* Lifecycle */

    componentDidMount() {
        this.pollJobState()
            .then(report => console.debug('first poll', report))
    }

    componentWillUnmount() {
        this.poll = false
    }

    readonly pollJobState = () =>
        this.poll
            ? this.api.report()
                .then(report => {
                    this.updateJobState(report)
                    setTimeout(() =>
                            this.pollJobState()
                                .catch(reason => console.debug('polling stopped due to', reason)),
                        POLL_RATE)
                    return report
                })
            : Promise.reject('poll flag unset')

    readonly updateJobState = ({up, tasks}: IJobReport) =>
        this.setState({up, tasks})

    /* Events */

    readonly toggleJob = (up: boolean) => {
        (up ? this.api.up() : this.api.down())
            .then(report => this.updateJobState(report))
    }

    /* View */

    readonly render = () =>
        <Router>
            <main className="container">
                <h1>
                    <Link to="/">Semantic Analysis</Link> {}
                    <JobIndicator up={this.state.up} onToggle={this.toggleJob}/>
                </h1>
                <Switch>
                    <Route path="/results/:id">{
                        ({match}) =>
                            <ResultDetail api={this.api} id={match!.params.id}/>
                    }</Route>
                    <Route path="/results">{
                        ({location}) => {
                            const params = new URLSearchParams(location.search)
                            return <ResultBrowser api={this.api} paginate={{
                                page: params.has('page') ? parseInt(params.get('page')!) : undefined,
                                perPage: params.has('perPage') ? parseInt(params.get('perPage')!) : undefined,
                            }} siteId={params.has('siteId') ? parseInt(params.get('siteId')!) : undefined}/>
                        }
                    }</Route>
                    <Route path="/">
                        <Dashboard api={this.api}
                                   up={this.state.up}
                                   tasks={this.state.tasks}
                                   updateJobState={this.updateJobState}
                        />
                    </Route>
                </Switch>
            </main>
        </Router>

}
