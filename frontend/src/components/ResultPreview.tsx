import * as React from 'react'
import {Logs} from './Logs'
import {IResultSimple, IItemSimple} from 'semantic-analysis'
import { isUrl } from 'semantic-analysis/lib/structured-data/item/spec'

export const ResultPreview: React.FunctionComponent<IResultSimple> = (props) => (
    <div className="ResultPreview">
        {!!props.logs.length && <Logs logs={props.logs}/>}

        {props.items.map((item, iIdx) => (
            <div key={iIdx}>
                <ItemInternal item={item} techBadge={true}/>
            </div>
        ))}
    </div>
)

const ItemInternal: React.FunctionComponent<{ item: IItemSimple; techBadge?: boolean }> = (props) => (
    <div className="ItemInternal card mb-3">

        <div className="card-header">
            <ul className="Type nav nav-pills card-header-pills">
                {props.item.type.map((type, tIdx) =>
                    <li className="nav-item" key={tIdx}>
                        <span className="nav-link p-2">
                            <LinkWrap href={type}>
                                <ItemType type={type}/>
                            </LinkWrap>
                            {props.item.id && (
                                <LinkWrap href={props.item.id} className="badge badge-pill badge-secondary">
                                    id: {crop(props.item.id, 20)}
                                </LinkWrap>
                            )}
                        </span>
                    </li>
                )}
                {props.techBadge && (
                    <li className="nav-item ml-auto">
                        <span className="nav-link p-2">
                            <span className="badge badge-pill badge-primary">
                                {props.item.meta.type}
                            </span>
                        </span>
                    </li>
                )}
            </ul>
        </div>

        {!!props.item.meta.logs.length && (
            <div className="card-body">
                <Logs logs={props.item.meta.logs}/>
            </div>
        )}

        <div className="Properties">
            <ul className="list-group list-group-flush">
                {Object.getOwnPropertyNames(props.item.properties).map(name => (
                    <li className="list-group-item" key={name}>

                        <LinkWrap href={name}>
                            <ItemProp prop={name}/>
                        </LinkWrap>

                        <ul className="list-group list-group-flush">
                            {props.item.properties[name].map((prop, vIdx) => (
                                <li className="list-group-item" key={vIdx}>
                                    {prop.meta.logs.length
                                        ? <Logs logs={prop.meta.logs}/>
                                        : undefined
                                    }
                                    {typeof prop.value !== 'string'
                                        ? <ItemInternal item={prop.value}/>
                                        : prop.value}
                                </li>
                            ))}
                        </ul>

                    </li>
                ))}
            </ul>
        </div>

    </div>
)

const LinkWrap: React.FunctionComponent<{ href?: string; className?: string; children: React.ReactNode }> =
    (props) =>
        props.href && isUrl(props.href)
            ? (
                <a href={props.href}
                   target="_blank"
                   className={props.className}
                >
                    {props.children}
                </a>
            ) : (
                <span className={props.className}>
                {props.children}
            </span>
            )

const ItemType: React.FunctionComponent<{ type: string }> = (props) => (
    <span className="ItemType">
        <strong>{unSchemaOrg(props.type)}</strong>
        {' '}
        <span title={`Type: ${props.type}`} className="badge badge-pill badge-secondary">
            type
        </span>
    </span>
)

const ItemProp: React.FunctionComponent<{ prop: string }> = (props) => (
    <span className="ItemProp">
        <strong>{unSchemaOrg(props.prop)}</strong>
        {' '}
        <span title={`Property: ${props.prop}`} className="badge badge-pill badge-secondary">
            prop
        </span>
    </span>
)

const crop = (str: string, len: number = 40) =>
    str.length > len
        ? str.substr(0, len) + '...'
        : str

const prefix = 'http://schema.org/'
const unSchemaOrg = (type: string) =>
    type.startsWith(prefix) && type.length > prefix.length
        ? type.substr(prefix.length)
        : type
