import {IPaginate} from 'semantic-analysis-manager/lib'
import {Link} from 'react-router-dom'
import * as React from 'react'

type IProps = IPaginate & {
    total: number
    linkTo: string
    maxDiff?: number
}

export const Paginate: React.FunctionComponent<IProps> = (props) => {
    const maxDiff = props.maxDiff ?? 3
    const totalPages = Math.ceil(props.total / props.perPage)
    const pageList = Array.from(
        Array(2 * maxDiff + 1).keys(),
        v => v - maxDiff + props.page
    ).filter(v => v >= 1 && v <= totalPages)

    return <ul className="pagination justify-content-center">
        <li className={['page-item', props.page - 1 < 1 ? 'disabled' : ''].join(' ')}>
            <Link
                className="page-link"
                to={{
                    pathname: props.linkTo,
                    search: `?page=${props.page - 1}&perPage=${props.perPage}`,
                }}
            >Previous</Link>
        </li>
        {!pageList.includes(1) ?
            <li className="page-item">
                <Link
                    className="page-link"
                    to={{
                        pathname: props.linkTo,
                        search: `?page=${1}&perPage=${props.perPage}`,
                    }}
                >{1}</Link>
            </li> : undefined}
        {pageList.map(page =>
            <li key={page} className={['page-item', page === props.page ? 'active' : ''].join(' ')}>
                <Link
                    className="page-link"
                    to={{
                        pathname: props.linkTo,
                        search: `?page=${page}&perPage=${props.perPage}`,
                    }}

                >{page}</Link>
            </li>
        )}
        {!pageList.includes(totalPages) ?
            <li className="page-item">
                <Link
                    className="page-link"
                    to={{
                        pathname: props.linkTo,
                        search: `?page=${totalPages}&perPage=${props.perPage}`,
                    }}
                >{totalPages}</Link>
            </li> : undefined}
        <li className={['page-item', props.page + 1 > totalPages ? 'disabled' : ''].join(' ')}>
            <Link
                className="page-link"
                to={{
                    pathname: props.linkTo,
                    search: `?page=${props.page + 1}&perPage=${props.perPage}`,
                }}
            >Next</Link>
        </li>
    </ul>
}
