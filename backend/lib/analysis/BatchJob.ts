import * as cp from 'child_process'
import * as path from 'path'
import {IJobOptions, IJobState, IMessage} from './analysis'

export type IJobReport = Partial<IJobState> & {
    up: boolean
}

export class BatchJob {

    /* Properties */

    private process?: cp.ChildProcess
    private state?: IJobState
    private resolvers: Array<(state: IJobReport) => void> = []

    /* Control */

    readonly report = (): IJobReport => ({
        up: !!this.process,
        ...this.state,
    })

    readonly up = (options: IJobOptions): Promise<IJobReport> =>
        // initiate process
        (
            !this.process
                ? new Promise<cp.ChildProcess>(resolve => {
                    if (!this.process) {
                        this.process = cp.fork(path.join(__dirname, 'analysis.js'))
                        this.process.on('message', ({id, job}: IMessage) => {
                            if (job) {
                                // wait for its first response
                                resolve(this.process)
                                console.debug(`[${module.filename}] response received`, {id, job})
                                this.state = job
                                this.resolvers[id]?.(this.report())
                            }
                        })
                    }
                })
                : Promise.resolve(this.process)
        )
            .then(process => new Promise<IJobReport>(resolve =>
                // then send the request
                process.send({
                    id: this.resolvers.push(resolve) - 1,
                    options,
                })
            ))

    readonly down = () =>
        this.process?.kill() && !(this.process = undefined)

}
