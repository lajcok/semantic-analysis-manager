Semantic Analysis Manager
=========================

Semester Project, Jiří Lajčok LAJ0013, VŠB-TUO 2019/2020

First run
------------

1. clone / copy the repository \
`$ git clone git@bitbucket.org:lajcok/semantic-analysis-manager.git`

2. enter it \
`$ cd semantic-analysis-manager`

3. backend

    1. enter backend directory \
    `$ cd backend`

    2. install dependencies \
    `$ npm install`

    3. copy environment settings \
    `$ cp .env.default .env`

    4. run database server \
    `$ docker-compose up`
         
    5. run TypeScript compile watcher \
    `$ npm run watch`
    
    6. create database schema \
    `$ node lib/models/schema.js --refresh`
    
    7. create local npm package link \
    `$ npm link`
    
    8. run API \
    `$ npm run api`

4. frontend 

    1. enter frontend directory \
    `$ cd frontend`
    
    2. install dependencies \
    `$ npm install`
    
    3. symlink backend \
    `$ npm link semantic-analysis-manager`

    4. run dev server \
    `$ npm run serve`
    
Sample dataset
--------------

Run SQL file with sample data, enter `semantics` as password \
`$ psql semantics -h localhost -p 5432 -U semantics -f backend/sql/sample_data.sql` \
