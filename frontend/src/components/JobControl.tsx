import * as React from 'react'
import {FormEvent} from 'react'
import {JobIndicator} from './JobIndicator'
import {IJobOptions} from 'semantic-analysis-manager/lib/analysis/analysis'
import {Spinner} from './Spinner'

interface IProps {
    up?: boolean
    onSubmit?: (options: IJobOptions) => Promise<boolean>
}

interface IState {
    processing: boolean
    urls: string
    additional: boolean
    rateLimit: number
    maxRunning: number
    crawl: boolean
    perSiteLimit: number
}

export class JobControl extends React.Component<IProps, IState> {

    /* Initialization */

    constructor(props: IProps) {
        super(props)
        this.state = {
            processing: false,
            urls: '',
            additional: true,
            rateLimit: 500,
            maxRunning: 1,
            crawl: false,
            perSiteLimit: 0,
        }
    }

    /* Events */

    readonly onSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        const urls = this.state.urls
            .split('\n')
            .filter(url => url !== '')

        const options: IJobOptions = {
            urls,
            rateLimit: this.state.rateLimit,
            maxRunning: this.state.maxRunning,
            crawl: this.state.crawl,
        }

        this.setState({processing: true})
        this.props.onSubmit?.(options)
            .then(ok => ok && this.setState({urls: ''}))
            .finally(() => this.setState({processing: false}))
    }

    /* View */

    readonly render = () => (
        <section className="card">
            <h5 className="card-header">
                Job Control <JobIndicator up={this.props.up}/>
            </h5>
            <form className="card-body" onSubmit={this.onSubmit}>
                <div className="form-group card">
                    <div className="card-body">
                        <h6 className="card-title" style={{cursor: 'default'}}
                            onClick={() => this.setState({additional: !this.state.additional})}
                            title={this.state.additional ? 'Collapse' : 'Expand'}
                            aria-label={this.state.additional ? 'Collapse' : 'Expand'}
                        >
                            {this.state.additional ? '\u25B4' : '\u25BE'} Batch Options
                        </h6>
                        {this.props.up
                            ? <p className="card-subtitle small text-muted mb-3">Stop current job to make changes</p>
                            : undefined
                        }
                        {this.state.additional ? <div>
                            <label className="row">
                                <span className="col-md-3 col-form-label col-form-label-sm">Rate limit</span>
                                <div className="col-md-9 input-group input-group-sm">
                                    <input type="number"
                                           className="form-control form-control-sm"
                                           disabled={this.props.up || this.state.processing}
                                           onChange={({target}) =>
                                               this.setState({rateLimit: parseInt(target.value)})
                                           }
                                           value={this.state.rateLimit}
                                           min={0} step={50}
                                    />
                                    <div className="input-group-append">
                                        <span className="input-group-text">ms</span>
                                    </div>
                                </div>
                            </label>
                            <label className="row">
                                <span className="col-md-3 col-form-label col-form-label-sm">
                                    Max running at a time
                                </span>
                                <div className="col-md-9">
                                    <input type="number"
                                           className="form-control form-control-sm"
                                           disabled={this.props.up || this.state.processing}
                                           onChange={({target}) =>
                                               this.setState({maxRunning: parseInt(target.value)})
                                           }
                                           value={this.state.maxRunning}
                                           min={1}
                                    />
                                </div>
                            </label>
                            <label className="form-check m-0">
                                <input type="checkbox"
                                       className="form-check-input"
                                       onChange={({target}) =>
                                           this.setState({crawl: target.checked})
                                       }
                                       disabled={this.props.up || this.state.processing}
                                />
                                <span>Crawl</span>
                            </label>
                            {this.state.crawl
                                ? <label className="row justify-content-center">
                                    <span className="col-md-3 col-form-label col-form-label-sm">
                                        Samples per site limit
                                    </span>
                                    <div className="col-md-8 input-group input-group-sm">
                                        <input type="number"
                                               className="form-control form-control-sm"
                                               disabled={this.props.up || this.state.processing}
                                               onChange={({target}) =>
                                                   this.setState({
                                                       perSiteLimit: parseInt(target.value) || 0
                                                   })
                                               }
                                               value={this.state.perSiteLimit}
                                               min={0} step={50}
                                        />
                                        <div className="input-group-append">
                                            <button type='button'
                                                    className="btn btn-outline-danger"
                                                    onClick={() => this.setState({perSiteLimit: 0})}
                                                    disabled={this.state.perSiteLimit === 0}
                                            >&times; cancel</button>
                                        </div>
                                    </div>
                                </label>
                                : undefined}
                        </div> : undefined}
                    </div>
                </div>
                <div className="form-group">
                    <label className="d-block">
                        <span>Entry URLs (one per line)</span>
                        <textarea className="form-control"
                                  rows={5}
                                  placeholder={'http://example.com\nhttp://example.com/page1\n...'}
                                  value={this.state.urls}
                                  onChange={event => this.setState({urls: event.target.value})}
                                  disabled={this.state.processing}
                        />
                    </label>
                </div>
                <button type="submit"
                        className="btn btn-primary"
                        disabled={this.state.processing}
                >Go {this.state.processing && <Spinner small/>}
                </button>
            </form>
        </section>
    )

}