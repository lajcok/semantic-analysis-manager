import * as express from 'express'
import {BatchJob} from './analysis/BatchJob'
import * as db from './models/db'
import * as stats from './models/statistics'

require('dotenv').config()


/* Express */

const baseUrl = new URL(`http://${process.env.API_HOST}:${process.env.API_PORT}/`)
const app = express()

app.use(express.json())


/* Job */

const job = new BatchJob()
app
    .get('/job', (req, res) => {
        return res.send(job.report())
    })
    .post('/job', async (req, res) => {
        const report = await job.up(req.body)
        return res.send(report)
    })
    .delete('/job', (req, res) => {
        job.down()
        return res.send(job.report())
    })


/* Results */

app
    .get('/results', async (req, res) => {
        const results = await db.listResults({
            paginate: {
                page: req.query.page ? parseInt(req.query.page as string) : undefined,
                perPage: req.query.perPage ? parseInt(req.query.perPage as string) : undefined,
            },
            siteId: req.query.siteId ? parseInt(req.query.siteId as string) : undefined,
        })
        console.debug('Requested result list', results)
        return res.send(results)
    })
    .get('/results/:id', async (req, res) => {
        const result = await db.getResult(parseInt(req.params.id))
        console.debug('Requested result details', result)
        return res.send(result)
    })


/* Statistics */

app
    .get('/stats/site', async (req, res) => {
        const result = await stats.getSiteStats()
        console.debug('Site Stats', result)
        res.send(result)
    })
    .get('/stats/type', async (req, res) => {
        const result = await stats.getTypeStats()
        console.debug('Type Stats', result)
        res.send(result)
    })
    .get('/stats/correctness/site', async (req, res) => {
        const result = await stats.getCorrectnessStats()
        console.debug('Correctness stats', result)
        res.send(result)
    })
    .get('/stats/correctness/hist', async (req, res) => {
        const result = await stats.getCorrectnessHist()
        console.debug('Correctness histogram', result)
        res.send(result)
    })


app.listen(process.env.API_PORT, () => console.log(`Server is running at ${baseUrl}`))
