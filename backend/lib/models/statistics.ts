/**
 * Statistic queries
 */

import {sequelize} from './schema'
import {queryResults, resultsQuery} from './db'

export interface ISiteStat {
    host: string
    total: number
    semantic: number
    errorResults: number
    errorLogs: number
}

export const getSiteStats = (): Promise<readonly ISiteStat[]> =>
    sequelize.query(`
        WITH resultsTotal AS (
            SELECT "siteId", COUNT(*) AS "resultCount"
            FROM results
            GROUP BY "siteId"
        ),
             resultsUnempty AS (
                 SELECT "siteId", COUNT(*) AS "resultCount"
                 FROM results
                 WHERE id IN (SELECT "resultId" FROM items)
                 GROUP BY "siteId"
             ),
             errorLogs AS (
                 SELECT r."siteId",
                        COUNT(DISTINCT r.id) AS resultCount,
                        COUNT(l.id)          AS logCount
                 FROM results r
                          INNER JOIN logs l
                                     ON r.id = l."resultId"
                                         AND l.sort IN ('Error', 'Warning')
                 GROUP BY r."siteId"
             )
        SELECT s.id,
               s.host,
               coalesce(rt."resultCount", 0) AS "resultsTotal",
               coalesce(ru."resultCount", 0) AS "resultsUnempty",
               coalesce(re.resultCount, 0)   AS "resultsWithErrors",
               coalesce(re.logCount, 0)      AS "errorLogs"
        FROM sites AS s
                 FULL JOIN resultsTotal AS rt
                           ON s.id = rt."siteId"
                 FULL JOIN resultsUnempty AS ru
                           ON s.id = ru."siteId"
                 FULL JOIN errorLogs AS re
                           ON s.id = re."siteId"
        ORDER BY s.host;
    `)
        .then(([siteStats, ..._]: [any[]]) =>
            siteStats.map(({host, resultsTotal, resultsUnempty, resultsWithErrors, errorLogs}) => ({
                host: host ?? 'unassigned',
                total: parseInt(resultsTotal),
                semantic: parseInt(resultsUnempty),
                errorResults: parseInt(resultsWithErrors),
                errorLogs: parseInt(errorLogs),
            }))
        )

export interface ITypeStat {
    siteHost: string | undefined
    typeHost: string | undefined
    total: number
    min: number
    max: number
    avg: number
}

export const getTypeStats = (): Promise<readonly ITypeStat[]> =>
    sequelize.query(`
        WITH itemTypes AS (
            SELECT *, split_part(unnest(types), '://', 2) AS "itemType"
            FROM items
            UNION
            SELECT *, NULL AS "itemType"
            FROM items
            WHERE types = '{}'
        ),
             resultCounts AS (
                 SELECT r.*, i."itemType", count(*) AS "itemCount"
                 FROM itemTypes i
                          INNER JOIN results r
                                     ON i."resultId" = r.id
                 GROUP BY r.id, i."itemType"
                 ORDER BY r.id, i."itemType"
             )
        SELECT s.host, r."itemType",
               SUM("itemCount")::INT AS "total",
               MIN("itemCount")::INT AS "min",
               MAX("itemCount")::INT AS "max",
               AVG("itemCount")::FLOAT AS "avg"
        FROM resultCounts r
                 INNER JOIN sites s
                            ON r."siteId" = s.id
        GROUP BY s.host, r."itemType"
        ORDER BY r."itemType" NULLS FIRST, s.host NULLS FIRST;
    `)
        .then(([typeStats, ..._]: any[]) => typeStats.map(
            ({host, itemType, total, min, max, avg}: any) => ({
                siteHost: host,
                typeHost: itemType,
                total, min, max, avg
            })))

export interface ISiteCorrectness {
    host: string
    min: number
    max: number
    mean: number
}

export const getCorrectnessStats = (): Promise<readonly ISiteCorrectness[]> =>
    sequelize.query(`
        WITH correctness AS (
            ${resultsQuery({})}
        )
        SELECT s.host,
               MIN("correctness") AS "min",
               MAX("correctness") AS "max",
               AVG("correctness") AS "mean"
        FROM correctness c
            INNER JOIN sites s
                ON c."siteId" = s.id
        GROUP BY s.id
        ORDER BY s.host;
    `)
        .then(([stats]: [any[]]) =>
            stats.map(({host, min, max, mean}) => ({
                host, min, max, mean
            }))
        )

export interface IHistData {
    bucket: number
    frequency: number
    interval: [number | undefined, number | undefined]
    mean: number | undefined
}

export const getCorrectnessHist = (nBuckets = 15): Promise<readonly IHistData[]> =>
    sequelize.query(`
        WITH correctness AS (
            ${resultsQuery({})}
        )
        SELECT width_bucket("correctness", 0., 1., :seps) AS "bucket",
               MIN("correctness") AS "intervalLow",
               MAX("correctness") AS "intervalHigh",
               AVG("correctness") AS "mean",
               COUNT(*)::INT AS "frequency"
        FROM correctness
        GROUP BY "bucket"
        ORDER BY "bucket" NULLS FIRST;
    `, {replacements: {seps: nBuckets - 1}})
        .then(([buckets]: [any[]]) => buckets
            .map(({bucket, intervalLow, intervalHigh, mean, frequency}) => ({
                bucket, frequency,
                interval: [intervalLow, intervalHigh],
                mean,
            }))
        )

if (module === require.main) {
    getSiteStats().then(siteStats => console.log({siteStats}))
    getTypeStats().then(typeStats => console.log({typeStats}))
}
