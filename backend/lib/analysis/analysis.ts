/**
 * Script to be run as a process
 */

import {IBatchState, ITaskArchived, toSerializable} from 'semantic-analysis'
import Exporter from 'semantic-analysis/lib/tools/Exporter'
import {storeResult} from '../models/db'
import {ILazyOptions, LazyAnalyzer} from './LazyAnalyzer'
import {VisitLogDb} from './VisitLogDb'


export interface IMessage {
    id?: any
    options?: IJobOptions
    job?: IJobState
}

export interface IJobOptions {
    urls?: ReadonlyArray<string>
    rateLimit?: number
    crawl?: boolean
    maxRunning?: number
    perSiteLimit?: number
}

export interface IJobState {
    tasks: IBatchStateSimple
}


/* Initialization */

const outDir = 'exports/'
const exporter = new Exporter(outDir)
const visitLog = new VisitLogDb()

const analyzer = new LazyAnalyzer({
    exporter, visitLog,
    doneCallback: done =>
        storeResult(toSerializable(done.results), done)
            .then(() =>
                console.log(`[${done.url}] Finished task`,
                    done.exportInfo ? `& exported to ${done.exportInfo.json}` : '')
            )
            .catch((error: any) =>
                console.error(`[${done.url}] Could not sore task due to an error`, error)
            ),
    failedCallback: failed =>
        console.error(`[${failed.url}] Task failed`, failed),
    onTaskUpdate: batch => sendState(batch),
})

visitLog.initiate()
    // .then(() => console.debug('visit log initialized', visitLog))
    .then(() => sendState(analyzer.state()))

// receiver
process.on('message', ({id, options}: Partial<IMessage>) => {
    if (options) {
        console.debug(`[${module.filename}] options received`, options)

        if (!analyzer.up()) {
            analyzer.passOptions((
                ({crawl, maxRunning, rateLimit, perSiteLimit}: IJobOptions): Partial<ILazyOptions> =>
                    ({crawl, maxRunning, rateLimit, perSiteLimit})
            )(options))
        }
        options.urls?.forEach(url => analyzer.addTask(url))

        sendState(analyzer.state(), id)
    }
})

// sender
const sendState = (batch: IBatchState, id?: any) =>
    process.send?.({
        id, job: {tasks: simplifyBatch(batch)}
    })

process.on('exit', code => {
    console.log('Disposing analyzer...')
    analyzer.dispose()
    console.log('Done, exiting...')
})


/* Simplification for message passing */

export type IBatchStateSimple = {
    queued: ReadonlyArray<ITaskSimple>
    running: ReadonlyArray<ITaskSimple>
    done: ReadonlyArray<ITaskSimple>
    failed: ReadonlyArray<ITaskSimple>
    queuedTotal: number
    runningTotal: number
    doneTotal: number
    failedTotal: number
}

export type ITaskSimple = Pick<ITaskArchived, 'url' | 'error' | 'exportInfo'>

export const simplifyTask = ({url, error, exportInfo}: ITaskArchived): ITaskSimple =>
    ({url, error, exportInfo})

export const simplifyBatch = (batch: IBatchState): IBatchStateSimple => {
    const simple: any = {}
    Object.entries(batch).forEach(([key, value]) => {
        simple[key] = value.map(simplifyTask).slice(0, 10)
        simple[`${key}Total`] = value.length
    })
    return simple as IBatchStateSimple
}
