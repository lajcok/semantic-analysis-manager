/**
 * Database Schema definitions
 *
 * TODO Adjust for TypeScript: https://sequelize.org/master/manual/typescript.html or use `sequelize-typescript`
 */
const {Sequelize, DataTypes} = require('sequelize')
require('dotenv').config()


export const sequelize = new Sequelize(
    process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
        host: process.env.DB_HOST,
        dialect: 'postgres',
    }
)


/* Presets */

const maxUrlLength = 2048
const maxTermLength = 512


/* Semantic Data */

const sort = DataTypes.ENUM('Notice', 'Warning', 'Error')
const phase = DataTypes.ENUM('Parsing', 'Extraction', 'Semantics')
const tech = DataTypes.ENUM('JSON-LD', 'Microdata', 'RDFa', 'Microformats')

export const Log = sequelize.define('log', {
    sort: {type: sort, allowNull: false},
    message: {type: DataTypes.TEXT, allowNull: false},
    phase: {type: phase, allowNull: false},
    term: {type: DataTypes.STRING(maxTermLength), allowNull: true},
}, {timestamps: false})

export const Meta = sequelize.define('meta', {
    type: {type: tech, allowNull: true},
}, {
    timestamps: false,
    name: {
        singular: 'meta',
        plural: 'metas',
    },
})

Log.belongsTo(Meta)
Meta.hasMany(Log)

export const Result = sequelize.define('result', {
    url: {type: DataTypes.STRING(maxUrlLength), allowNull: false},
    html: {type: DataTypes.STRING(512), allowNull: true},
    json: {type: DataTypes.STRING(512), allowNull: true},
}, {
    timestamps: true,
    createdAt: 'done',
    updatedAt: false,
})

Meta.belongsTo(Result)
Result.hasOne(Meta)
Log.belongsTo(Result)
Result.hasMany(Log)

export const Item = sequelize.define('item', {
    types: {
        type: DataTypes.ARRAY(DataTypes.STRING(maxTermLength)),
        allowNull: false,
    },
    itemUri: {type: DataTypes.STRING(maxUrlLength), allowNull: true},
}, {timestamps: false})

Meta.belongsTo(Item)
Item.hasOne(Meta)
Item.belongsTo(Result)
Result.hasMany(Item)

export const Property = sequelize.define('property', {
    name: {type: DataTypes.STRING(maxTermLength), allowNull: false},
    value_string: {type: DataTypes.TEXT, allowNull: true},
}, {timestamps: false})

Property.belongsTo(Item, {foreignKey: 'value_item'})
Property.belongsTo(Item)
Item.hasMany(Property)
Meta.belongsTo(Property)
Property.hasOne(Meta)

export const Site = sequelize.define('site', {
    host: {type: DataTypes.STRING(256), allowNull: false, unique: true},
}, {timestamps: false})

Result.belongsTo(Site)
Site.hasMany(Result)


export const Collection = sequelize.define('collection', {
    name: {type: DataTypes.STRING(20)},
}, {timestamps: false})

Result.belongsTo(Collection, {
    foreignKey: {
        defaultValue: 1
    }
})
Collection.hasMany(Result)


// TODO Include term definition vocabulary


if (module == require.main && process.argv.includes('--refresh')) {
    sequelize.sync({force: true})
        .then(() =>
            Collection.create({
                id: 1,
                name: 'Default',
            }).then(({id}: any) =>
                console.debug('Default collection created with id:', id)
            )
        )
}
